// This is a node.js script that will help compile your Hogan.js templates
// npm install hogan.js before you run this script to compile your templates

// USAGE: node compileTemplates.js <folder1> <folder2>
// where <folder1>, <folder2> represent folders that contain your templates.

var Hogan = require('hogan.js'),
  fs = require('fs'),
  args = process.argv,
  DEBUG = false;

if (!Hogan) {
  throw new Error('Did you npm install hogan.js correctly ?');
}

// remove 'node' and 'compileTemplates.js' from the picture
// all other folders are template folders
var templateFolders = args.splice(2);

console.log('Compiling templates for : ', templateFolders);

try {
  templateFolders.forEach(function (directory) {
    // compile templates for every directory passed in
    // write the compiled output to the same directory location
    var contents = fs.readdirSync(directory);
    if (DEBUG) {
      console.log('Directory contents : ', contents);
    }
    contents.forEach(function(f){
      var path = directory + '/' + f;
      var stats = fs.statSync(path);

      // proceed only if its an HTML file
      if (stats.isFile() && (f.indexOf('.html') >= 0)) {
        if (DEBUG) {
         console.log('Processing : ', path);
        }

        var fileContents = fs.readFileSync(path, 'UTF-8');
        var compiled = Hogan.compile(fileContents, {asString: true});

        // create an AMD'fied compiled template and write to file system
        var definedCompiled = 'define([], function () { return ' + compiled +'});';
        var outputName = directory + '/' + f.substring(0, f.lastIndexOf('.html')) + '.js';
        if (DEBUG) {
          console.log('Writing output to : ', outputName);
        }
        fs.writeFileSync(outputName, definedCompiled, 'UTF-8');
      }
    });
  });
  console.log('All done.');
} catch (e) {
  console.error('Failed to compile Hogan.js templates', e);
}