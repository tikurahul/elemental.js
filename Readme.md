# Elemental.js

Elemental.js is a simple framework that helps you write clean UI components that are AMD compliant.

## Salient Features

* Simple (gets out of your way pretty quickly).
* Internationalization (i18n) ready.
* Supports *resource provisioning* (different templates for portrait / landscape, languages, screen size, dpi etc).
* AMD compliant, so you can use any module loader of your choice. We prefer [Require.js](http://requirejs.org/ "Require.js").
* No build step (especially in development mode).

## What is it built on ?

* [Simple JavaScript Inheritance](http://ejohn.org/blog/simple-javascript-inheritance/ "Simple JavaScript Inheritance")
* [Require.js](http://requirejs.org/ "Require.js") for module loading
* [jQuery](http://api.jquery.com "jQuery")
* [Hogan.js for templating](http://twitter.github.com/hogan.js/ "Hogan.js")

### Organization of a component

Elemental.js helps you build UI components that are re-usable and extensible. A UI component looks like:

    Component
    |
    |_ js
    |  |
    |  |_ *.js (all javascript files)
    |
    |_ resources
    |  |
    |  |_ *.* (all other resources, hogan.js templates)
    |  |_ config.js (a javascript file enumerating all the included resources in the resoures folder)
    |
    |_ i18n
       |
       |_ strings.js
       |_ strings_DE.js
       |_ strings_FR.js (etc.)


Elemental.js follows convention over configuration. Every component has a *js* folder that defines all the
scripts used by the component.

The *resources* folder contains all the resources that your component may need.
This normally includes things like css files, text files, hogan.js / mustache templates etc.

Finally a component also contains an *i18n* folder that contains all the internationalized string resources.

### Quickstart

Let's define a simple login component.

    :::javascript
    define(['require','elemental.js/js/templated', '../resources/config'], function (require, Templated, config) {

      // defining a login component
       var Login = Templated.extend({

        init : function(context, properties) {
          this._super(require, context, properties, config);
          // do your initialization here
          this.$login = null;
        },

        setup: function () {
          var that = this;
          // load the necessary template and inflate the component with the corresponding template
          this.getTemplate('login.html', function(template) {
            var data = ...;
            // every hogan template has a .render() method
            that.$login = $(template.render(data));
            that.trigger('ready');
          });
        },

        node: function () {
          return this.$login;
        }

      });

      return Login;
    });

Now, to use this component, we can do the following:

    :::javascript
    require(['jquery', '../js/Login'], function ($, Login) {
      var $root = $('root'),
        login = new Login({'language': 'EN'});

      // when ready add the component to the DOM
      login.on('ready', function() {
        $root.append(login.node());
      });

      login.startup();
    });

### Internationalization (i18n)

The format of the string resource bundle, looks like:

**strings.js**

    :::javascript
    define({
      'txt_hello': 'Hello i18n'
    });

**strings_DE.js**

    :::javascript
    define({
      'txt_hello': 'Guten Tag i18n'
    });

For internationalized templates you can also use a *data-i18n* attribute, as well as an optional *data-i18n-attr* placeholder.
Here *data-i18n* attribute specifies the key (in strings.js) to be used. An optional *data-i18n-attr* represents the location for placement (in the DOM.). The default
*data-i18n-attr* is *innerHTML* or *$.html()*.

    :::html
    {{#messages}}
    <ul>
      <li>
        <span data-i18n='lbl_message'><!--default data-i18n-attr--></span>
        {{>message}}
      </li>
    </ul>
    {{/messages}}

In order to *trigger* the i18n-ification, all you need to do, is to call the *i18n()* method on the *Templated* component. For e.g.

    :::javascript
    define(['require','elemental.js/js/templated', '../resources/config'], function (require, Templated, config) {

      // defining a login component
       var Login = Templated.extend({

        init : function(context, properties) {
          // omitted for brevity...
        },

        setup: function () {
          var that = this;
          // load the necessary template and inflate the component with the corresponding template
          this.getTemplate('login.html', function(template) {
            var data = ...;
            // every hogan template has a .render() method
            that.$login = $(template.render(data));
            // i18n phase
            that.i18n();
            that.trigger('ready');
          });
        },

        node: function () {
          return this.$login;
        },

        // ... rest is omitted for brevity

      });

      return Login;
    });

### Resource Provisioning

*elemental.js* supports a resource provisioning approach for supporting responsive design. Sometimes, media queries are not responsive enough, and we need different templates
to be used in those cases.

Let's say you were designing a mobile website, and you wanted to show a carousel only in landscape mode. Your resources folder would then contain:

    resources
    |
    |_ carousel.html
    |_ carousel_landscape.html

elemental.js does the *heavy lifting* in determining which resource (template) to use based on the current *context*. This decision is based on the *resourcePriority()* method defined in
*component.js*, thus also making it *overridable*.

    :::javascript
    var template = null;

    // instead of writing code that looked like:
    if (this.context.orientation === 'landscape') {
      template = 'carousel_landscape.html';
    } else if(...) {
      ....
    }
     else {
      template = 'carousel.html';
    }

    // you just need to write:
    this.getTemplate('carousel.html'), function(template) {
      // at this point resource provisioning rules have been applied
      // and the best template has been chosen, based on the current
      // context.
    });

### *resources/config.js*

*config.js* contains a list of *all* enumerated filenames in the resource folder. This list of names is used for *resource provisioning*.

An example *config.js* could look like:

    :::javascript
    define({
      'resources': [
        'test.txt',
        'template.html', // default template
        'template_DE.html', // template provisioned when context.language === 'DE'
        'template_DE_large.html' // // template provisioned when context.language === 'DE' && context.screenSize === 'large'
      ]
    });

### Compiling Hogan.js templates

The *bin* folder has a script ***compileTemplates.js***, that compiles the Hogan.js templates that you define in your project. All you need to do is:

* Install Node.js if you have not already. For information, on how you can install Node.js for your platform visit [node.js](http://nodejs.org/ "Node.js")
* Install Hogan.js [Hogan.js](http://twitter.github.com/hogan.js/ "Hogan.js")
* Finally you can now run the ***compileTemplates.js*** script. Pass in the list of template folders, as command line arguments, and the compiled templates are generated in the same *templates* folder

        npm install hogan.js
        node compileTemplates.js folder1 folder2
