define(['require', 'jquery', '../lib/class', './i18n', '../lib/text', '../lib/hogan/hogan'], function(require, $, Class, i18n, text, Hogan) {
  'use strict';

  /**
  * Defines a basic component (which is essentially some HTML + JS + CSS).
  */
  var Component = Class.extend({

    /**
    * The base constructor function for all components.
    *
    * @param localRequire represents your local require context, that is used for provisioning resources
    * @param {Object} context represents a global context. Contains useful information like screen size, language etc
    * @param {Object} optional properties represents a couple of arbritrary properties that can be mixed into the component
    * @param {Object} represents your components resource config. Typically is present in ../resources/config.js
    * @param {String} represents an component root. By default its .. (as typically components are in the js/ folder)
    *   For nested folders you can use a path which points to the "src" directory (one above js/ directory)
    */
    init: function(localRequire, context, properties, resourceConfig, componentRoot) {
      // localRequire represents the localRequire for the AMD compliant component
      if(!localRequire || typeof(localRequire.toUrl) !== 'function') {
        // local require has not been specified
        // getStrings(), getTemplate() will NOT WORK
        throw new Error('Specify a local require, so that we can provision resources correctly');
      }

      this.localRequire = localRequire;

      // context represents the global context of the component
      // all components have a global context, that they can use to get useful
      this.context = context || {};

      // arbitrary properties that can be mixed into a component
      properties = properties || {};
      this.properties = {};

      var property;
      for(property in properties) {
        if(properties.hasOwnProperty(property)) {
          this.properties[property] = properties[property];
        }
      }

      // initialize event listeners
      // every component has its event listeners, that other components can subscribe to
      // {topic, [callback1, callback2]}
      this.eventListeners = {};
      this.resourceConfig = resourceConfig;

      // the default component root is .. (as *.js files are normally in ../js/*)
      // setup resource paths and i18n paths
      // default componentRoot is .. (relative to the /js folder in the component)
      this.i18nPath = componentRoot ? (componentRoot + '/i18n/strings') : '../i18n/strings';
      this.resourcePath = componentRoot ? (componentRoot + '/resources/') : '../resources/';
    },

    /**
    * Defines a resource provisioning priority.
    *
    * @return {Object} that represents a map of pivots (that are represented as keys in the component context) with possible values in their priority order.
    *
    * Example:
    * <code>
    * {
    *   'language': ['DE', 'FR', 'ES', 'IT'],
    *   'orientation': ['portrait', 'landscape'],
    *   'screenSize': ['small', 'medium', 'large', 'xlarge', 'xxlarge']
    * }
    * </code>
    */
    resourcePriority: function() {
      return {
        'language': ['DE', 'FR', 'ES', 'IT'],
        'orientation': ['portrait', 'landscape'],
        'screenSize': ['small', 'medium', 'large', 'xlarge', 'xxlarge']
      };
    },

    /*
    * Return the language that the component should be rendered in.
    *
    * Returns '' for EN (English) and for others returns the locale code
    * The component can OPTIONALLY override to change the language for the rendered component.
    * If the component overrides this method, it will need to follow the same contract
    *
    * @return {String} representing the language that the component is rendered in
    */
    language: function() {
      var language = this.context.language || '';
      return(language ? (language === 'EN' ? '' : language) : '');
    },

    /*
    * Returns the path to the i18n resource bundle (strings_*.js).
    *
    * @return {String}
    */
    i18nBundle: function() {
      var language = this.language();
      return this.i18nPath + (language ? ('_' + language) : '');
    },

    /**
    * This method can be used to fetch strings from the i18n resource bundle given its keys
    * @param {Array} keys represents the string keys
    * @param {Function} callback reprsents the callback function, when the strings are fetched
    *
    * @return {$.Deferred} a jquery deferred to chain callbacks
    */
    getStrings: function(keys, callback) {
      var d = new $.Deferred().done(callback),
        i18nResourcePath = this.i18nBundle();

      i18n.getStrings(d, this.localRequire, i18nResourcePath, keys);
      return d.promise();
    },

    // private method, strips the extension from the name of the resource
    // template.html --> template
    _stripExtension: function(name) {
      var index = name.lastIndexOf('.');
      return index >= 0 ? name.substring(0, index) : name;
    },

    /**
    * This method can be used to fetch templates from the resources directory. Applies the resource provisioned rules defines in the resourcePriority() method.
    *
    * @param {String} template represents the template name
    * @param {Function} callback reprsents the callback function, when the template has been successfully fetched
    *
    * @return {$.Deferred} a jquery deferred to chain callbacks
    */
    getTemplate: function(template, callback) {
      var d = new $.Deferred().done(callback),
        localRequire = this.localRequire,
        // dev mode ?
        devMode = this.context.development,
        isHtml = template.lastIndexOf('.html') >= 0,
        name = this._stripExtension(template),
        resource = this.getResource(name),
        fullResourceName = this.resourcePath + resource,
        compiledResourceName = this.resourcePath + this._stripExtension(resource),
        // using the text plugin's onLoad event handler
        // which is modelled in the following manner
        // onLoad will only get called when we have to compile a template
        onLoad = function(content) {
          // compile the template on behalf othe callee
          content = Hogan.compile(content);
          d.resolve(content);
        };

      // registering an errback
      onLoad.error = function(error) {
        d.reject(error);
      };

      if(devMode && isHtml) {
        // we are using the text plugin, in a non-plugin way
        text.load(fullResourceName, localRequire, onLoad, {
          isBuild: false
        });
      } else {
        // compiled template
        // use the local require to make a request for the compiled template
        localRequire([compiledResourceName], function(r) {
          d.resolve(new Hogan.Template(r));
        });
      }

      return d.promise();
    },

    /**
    * Returns a resource name to be loaded based on resource provisioning rules.
    * If you want to get access to a template use getTemplate() instead
    *
    * @return {String} resource name
    */
    getResource: function(name) {
      // load resourceConfig, only when someone asks for a resource, otherwise this will never get initialized
      if(!this.resourceConfig) {
        return '';
      }

      var that = this,
        // represents all the resource available, as defined by the component
        resources = this.resourceConfig.resources || [],
        applicableResources = [],
        priority = this.resourcePriority(),
        i = 0,
        matrix = null,
        max = 0,
        bestResource = null,
        resource = null,

        // eliminate resource files that contradict the device configuration
        // the resource should start with the same name, and if it has a pivot
        // that contradicts the device configuration, it is eliminated from the search list
        // e.g. if we were looking for template.html, then foo.html would be eliminated,
        // because it does not have the same resource name; and template_landscape.html would
        // be eliminated, if the device configuration was not in landscape.
        eliminate = function(resource, name) {
          var i = 0,
            pivot = null,
            resourceParts = null;

          resourceParts = that._stripExtension(resource).split('_');
          // does the resource start with the name we are looking for ?
          // if not eliminate = true
          if(resourceParts[0] !== name) {
            return true;
          }

          // look at the other parts of the resource name, find the pivot that they represent
          // and check if the device characteristics (as represented by the context) are what
          // we expect to see. if not, eliminate = true otherwise eliminate = false
          // e.g we saw 'FR' then the pivot which it represents = 'langugage', and if context[language] != 'FR'
          // then eliminate.
          for(i = 1; i < resourceParts.length; i = i + 1) {
            pivot = findPivot(resourceParts[i]);
            if(pivot) {
              if(resourceParts[i] !== that.context[pivot]) {
                return true;
              }
            }
          }
          return false;
        },

        // looks at a resource part and determines which pivot it corresponds to
        // e.g FR => language
        // e.g portrait => orientation
        findPivot = function(part) {
          var pivot = null,
            values = null,
            i = 0;

          for(pivot in priority) {
            if(priority.hasOwnProperty(pivot)) {
              values = priority[pivot];
              for(i = 0; i < values.length; i = i + 1) {
                if(part === values[i]) {
                  return pivot;
                }
              }
            }
          }
        },

        // once we have a list of applicable resource
        // we traverse though this list, and then assign then a value, based on how many
        // conditions they satisfy, in the resourcePriority defined
        // e.g. will return a map that has a resource name and a value (greater the value, more pivots it satisfies)
        // {"template_landscape.html":100,"template_landscape_DE.html":1100}
        getBestFit = function(applicableResources) {
          var matrix = {},
            pivot = null,
            resource = null,
            resourceParts = null,
            factor = 1000,
            value = null,
            i = 0;

          if(applicableResources && applicableResources.length === 1) {
            // return the highest priority
            matrix[applicableResources[0]] = 1000;
            return matrix;
          }

          for(i = 0; i < applicableResources.length; i = i + 1) {
            // reset factor
            factor = 1000;
            resource = applicableResources[i];
            resourceParts = that._stripExtension(resource).split('_');
            for(pivot in priority) {
              if(priority.hasOwnProperty(pivot) && that.context[pivot]) {
                value = that.context[pivot];
                if(resourceParts.indexOf(value) >= 0) {
                  matrix[resource] = (matrix[resource] || 0) + factor;
                }
                factor = factor / 10;
              }
            }
          }

          return matrix;
        };

      // eliminate resources that contradict device configuration
      for(i = 0; i < resources.length; i = i + 1) {
        if(!eliminate(resources[i], name)) {
          applicableResources.push(resources[i]);
        }
      }

      // calculate best fit
      matrix = getBestFit(applicableResources);

      // pick the largest value in the best fit matrix
      if(matrix) {
        for(resource in matrix) {
          if(matrix.hasOwnProperty(resource) && max < matrix[resource]) {
            max = matrix[resource];
            bestResource = resource;
          }
        }
      }

      return bestResource;
    },

    /**
    * This implementor of a component must override this method.
    * Typically the component will load a necessary template, and inflate itself, with the corresponding template and some data.
    */
    setup: function() {
      // the implementor of the component MUST override this method, and
      // load the necessary template and inflate the widget with the corresponding
      // template
    },

    resume: function() {
      // the implementor of the component, can OPTIONALLY implement this method
      // this method is available to handle onResume() like events in Android
      // in case the component, needs to re-render a part of the page, it could
      // provide the implementation in this method. The base component, would automatically
      // wire onResume() like events to this callback
    },

    // the only method that would need to be called by the person using the component
    // ideally you should NEVER have to override this method
    startup: function() {
      this.trigger('pre-startup', [this]);
      this.setup();
      this.trigger('post-startup', [this]);
    },

    // removes a component from a page
    // ideally you should NEVER have to override this method
    teardown: function() {
      this.trigger('pre-teardown', [this]);
      this.destroy();
      this.trigger('post-teardown', [this]);
    },

    /**
    * This implementor of a component can optionally override this method.
    * Typically the component will cleanup state, and remove itself from the DOM on destroy()
    */
    destroy: function() {
      // does nothing for the base component
      // the implementor of a component can OPTIONALLY override this method, in case the component
      // wants to clean up state
    },

    /**
    * Attach a custom event listener on a component.
    *
    * @param {String} event represents the name of the event
    * @param {Function} callback represents the callback function when the {@link trigger} is called
    */
    on: function(event, callback) {
      if(typeof(callback) !== 'function') {
        return;
      }
      this.eventListeners[event] = this.eventListeners[event] || [];
      this.eventListeners[event].push(callback);
    },

    /**
    * Remove a custom event listener on a component.
    *
    * @param {String} event represents the name of the event
    * @param {Function} callback represents the callback function to be removed
    */
    off: function(event, callback) {
      var eventListeners = this.eventListeners[event] || [],
        i;
      for(i = 0; i < eventListeners.length; i = i + 1) {
        if(callback === eventListeners[i]) {
          eventListeners = eventListeners.splice(i, 1);
        }
      }
    },

   /**
    * Triggers an event.
    *
    * @param {String} event represents the name of the event
    * @param {Array} args represent the arguments that can be passed on to the subscribers of this event
    */
    trigger: function(event, args) {
      var eventListeners = this.eventListeners[event] || [],
        i;
      for(i = 0; i < eventListeners.length; i = i + 1) {
        eventListeners[i].apply(this, [].concat(args));
      }
    }
  });

  return Component;

});