// this is modelled after a require.js plugin for i18n
define({
  getStrings: function(deferred, localRequire, i18nResourcePath, identifiers) {
    if(!localRequire || typeof(localRequire.toUrl) !== 'function') {
      deferred.reject('No local require, cannot provision strings');
    }

    if(!i18nResourcePath) {
      deferred.reject('No i18n resourcePath, cannot provision strings');
    }

    localRequire([i18nResourcePath], function(r) {
      var i = 0,
        result = {};

      for(i = 0; i < identifiers.length; i = i + 1) {
        result[identifiers[i]] = r[identifiers[i]];
      }

      deferred.resolve(result);
    });
  }
});