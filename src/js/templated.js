define(['jquery', './component'], function($, Component) {
  'use strict';

  // extends the base component to provide an implementation of a templated component
  // use this component when you are designing a component that needs a template
  var Templated = Component.extend({
    init: function(require, context, properties, resourceConfig, componentRoot, template) {
      this._super(require, context, properties, resourceConfig, componentRoot);
      this.template = template;

      // node will be lazy loaded before the template is about to be rendered
      this.$cnode = null;
    },

    // templated component will automatically used the cached sizzled selector
    // if you are using other sub-elements you will NEED to cached the sizzled selectors yourself
    $cachedNode: function() {
      if(!this.$cnode) {
        this.$cnode = this.node();
      }
      return this.$cnode;
    },

    node: function() {
      // inflate template, based on resource provisioning rules
    },

    // transforms the data-i18n attributes to locale specfic string bundles
    i18n: function() {
      var $node = this.$cachedNode(),
        i18nNodes = [],
        // cached nodes
        $i18nNodes = [],
        $i18nNode = null,
        // replacements
        keys = [],
        i = 0;

      if($node) {
        i18nNodes = $node.find('*[data-i18n]');
        // collection phase
        for(i = 0; i < i18nNodes.length; i = i + 1) {
          $i18nNode = $(i18nNodes[i]);
          // collect all the i18n keys and attributes
          $i18nNodes.push($i18nNode);
          keys.push($i18nNode.data('i18n'));
        }

        this.getStrings(keys, function(r) {
          var key = null,
            attribute = null;

          // replacement phase
          for(i = 0; i < $i18nNodes.length; i = i + 1) {
            key = $i18nNodes[i].data('i18n');
            if(key && r[key]) {
              // default to the text attribute
              attribute = $i18nNodes[i].data('i18n-attr') || '';
              if(attribute) {
                if(attribute.toLowerCase() === 'html') {
                  $i18nNodes[i].html(r[key]);
                } else {
                  $i18nNodes[i].attr(attribute, r[key]);
                }
              } else {
                $i18nNodes[i].text(r[key]);
              }
            }
          }
        });
      }
    },

    // provide a default destroy, which overrides the destroy() in the base component
    destroy: function() {
      var $node = this.node();
      if($node) {
        $node.remove();
      }
    }

  });

  return Templated;

});